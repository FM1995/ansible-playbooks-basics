# Ansible Playbooks basics

#### Project Outline

Now with Ansible being able to ping servers is all well and good, we also want to configure servers and we can do that with Ansible Playbooks


#### Lets get started

Lets begin with the hosts file, which will contain the managed nodes to target and tasks to execute

And can see it’s been done like the below, where it is targeting our digital ocean droplets

![Image1](https://gitlab.com/FM1995/ansible-playbooks-basics/-/raw/main/Images/Image1.png)

Now lets configure our playbook, where it will have ordered list of tasks or plays

Lets process to create a YAML file for the playbook

![Image2](https://gitlab.com/FM1995/ansible-playbooks-basics/-/raw/main/Images/Image2.png)

Renaming hosts to webserver in the hosts file and then referencing it in the playbook

Can then configure the tasks to execute nginx using the modules. In the module have used apt since we have ubuntu servers. Can then use another module called service

![Image3](https://gitlab.com/FM1995/ansible-playbooks-basics/-/raw/main/Images/Image3.png)

Now lets execute the playbook

Before we do that we need to configure the hosts

![Image4](https://gitlab.com/FM1995/ansible-playbooks-basics/-/raw/main/Images/Image4.png)

Now ready to execute the playbook using the below command

```
ansible-playbook -I hosts my-playbook.yaml
```

And can it is a success

![Image5](https://gitlab.com/FM1995/ansible-playbooks-basics/-/raw/main/Images/Image5.png)

Can then ssh in to one of the servers and check if they are running

And run the below to check for the nginx process running

```
ps aux | grep nginx
```

![Image6](https://gitlab.com/FM1995/ansible-playbooks-basics/-/raw/main/Images/Image6.png)

Now lets say we want install a specific version

Lets take the below for example with a link from the ubuntu installation page

https://launchpad.net/ubuntu

![Image7](https://gitlab.com/FM1995/ansible-playbooks-basics/-/raw/main/Images/Image7.png)

Can then configure the below in the playbook as well as inputting the state

![Image8](https://gitlab.com/FM1995/ansible-playbooks-basics/-/raw/main/Images/Image8.png)

And then execute the playbook

```
ansible-playbook -I hosts my-playbook.yaml
```

![Image9](https://gitlab.com/FM1995/ansible-playbooks-basics/-/raw/main/Images/Image9.png)

Can then also stop the nginx server by changing from started to stopped in the state

![Image10](https://gitlab.com/FM1995/ansible-playbooks-basics/-/raw/main/Images/Image10.png)








